# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

10.times { |work_id| Work.create(id: work_id, name: "name_#{work_id}", description: "description_#{work_id}") }
100.times { |gadget_id| Gadget.create(id: gadget_id, name: "name_#{gadget_id}", work_id: gadget_id % 10, description: "description_#{gadget_id}") }
