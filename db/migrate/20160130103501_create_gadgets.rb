class CreateGadgets < ActiveRecord::Migration[5.0]
  def change
    create_table :gadgets do |t|
      t.string :name, null: false
      t.integer :work_id, null: false
      t.string :description

      t.timestamps
    end
  end
end
