class TopController < ApplicationController
  def index
    @works = Work.all.order("id desc").limit(5)
    @gadgets = Gadget.includes(:work).all.order("id desc").limit(5)
  end
end
